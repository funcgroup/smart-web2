package cn.com.smart.form.enums;

import cn.com.smart.web.bean.LabelValue;
import com.mixsmart.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 搜索插件类型
 * @author 乌草坡 2019-09-01
 * @since 1.0
 */
public enum SearchPluginType {
    /**
     * cnoj-input-tree -- 树形
     */
    INPUT_PLUGIN_TREE(TextPluginType.TEXT_PLUGIN_TREE.getValue(), "树型插件"),

    /**
     * cnoj-auto-complete -- 自动完成
     */
    INPUT_PLUGIN_AUTO_COMPLETE(TextPluginType.TEXT_PLUGIN_AUTO_COMPLETE.getValue(), "自动完成插件"),

    /**
     * cnoj-input-select -- 输入框下拉框插件
     */
    INPUT_PLUGIN_SELECT(TextPluginType.TEXT_PLUGIN_SELECT.getValue(), "文本下拉框插件"),

    /**
     * 下拉框
     */
    SELECT("select", "下拉框"),

    /**
     * 日期时间控件
     */
    INPUT_PLUGIN_DATETIME("cnoj-datetime", "日期时间控件"),

    /**
     * 日期控件
     */
    INPUT_PLUGIN_DATE("cnoj-date", "日期控件"),

    /**
     * 时间控件
     */
    INPUT_PLUGIN_TIME("cnoj-time", "时间控件");

    private String value;

    private String text;

    SearchPluginType(String value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取搜索表达式类型对象
     * @param value 类型值
     * @return 返回类型对象
     */
    public static SearchPluginType getObj(String value) {
        if(StringUtils.isEmpty(value)) {
            return null;
        }
        SearchPluginType type = null;
        for(SearchPluginType typeTmp : SearchPluginType.values()) {
            if(typeTmp.getValue().equals(value)) {
                type = typeTmp;
                break;
            }
        }
        return type;
    }

    /**
     * 获取选项列表
     * @return 返回选项列表
     */
    public static List<LabelValue> getOptions() {
        List<LabelValue> labelValues = new ArrayList<LabelValue>();
        for(SearchPluginType typeTmp : SearchPluginType.values()) {
            labelValues.add(new LabelValue(typeTmp.getText(), typeTmp.getValue()));
        }
        return labelValues;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
