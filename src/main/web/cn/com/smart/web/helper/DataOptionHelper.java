package cn.com.smart.web.helper;

import java.util.ArrayList;
import java.util.List;

import com.mixsmart.utils.CollectionUtils;

import cn.com.smart.bean.SmartResponse;
import cn.com.smart.web.ISmartWeb;
import cn.com.smart.web.bean.LabelValue;

/**
 * 辅助类
 * @author lmq
 *
 */
public class DataOptionHelper {

    /**
     * 转换为数组列表
     * @param labelValues
     * @return
     */
   public static List<Object> toArrayList(List<LabelValue> labelValues) {
       if(CollectionUtils.isEmpty(labelValues)) {
           return null;
       }
       List<Object> objs = new ArrayList<Object>();
       for(LabelValue lv : labelValues) {
           objs.add(lv.toArray());
       }
       return objs;
   }

   /**
    * 处理Response结果
    * @param labelValues
    * @return
    */
   public static SmartResponse<Object> labelValueToResponse(List<LabelValue> labelValues) {
       SmartResponse<Object> smartResp = new SmartResponse<Object>();
       List<Object> objs = DataOptionHelper.toArrayList(labelValues);
       if(CollectionUtils.isNotEmpty(objs)) {
           smartResp.setDatas(objs);
           smartResp.setResult(ISmartWeb.OP_SUCCESS);
           smartResp.setMsg(ISmartWeb.OP_SUCCESS_MSG);
       }
       return smartResp;
   }
}
